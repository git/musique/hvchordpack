PREFIX ?= /usr/local

install:
	install -m 755 chordpack $(PREFIX)/bin/
	install -m 644 chordpro-mode.el $(PREFIX)/share/emacs/site-lisp/

uninstall:
	rm $(PREFIX)/share/emacs/site-lisp/chordpro-mode.el
	rm $(PREFIX)/bin/chordpack
