;;; Chordpro mode
;; Chordpro is a format of songs with guitar chords.
;;
;; Instalation:   1) copy this file to emacs load path
;;                2) add to your .emacs (load "chordpro-mode.el")
;;                3) add to mode to file extensions you want

(defun chordpro-mode-choir-around ()
   (interactive)
   "Surround region with start and end of choir marks..."
   (vim-filter "echo \"{soc}\";cat;echo \"{eoc}\""))

(defun call-key (key)
   "Call the command KEY is bound to. Example of usage in lisp: (call-key [f2])."
   (interactive)
   (command-execute (key-binding key) t))

(defvar chordpro-font-lock-keywords 
  ;; Here \n is for endline!
  '(("\\(\\[[^]]*\\]\\)"       1 'font-lock-string-face)
    ("^\\(#.*\\)"              1 'font-lock-comment-face)
    ("\\({subtitle[^}]*}\\)"   1 'font-lock-type-face)
    ("\\({title[^}]*}\\)"      1 'font-lock-keyword-face)
    ("\\({[^}]*}\\)"           1 'font-lock-variable-name-face)
    ))

(define-derived-mode chordpro-mode fundamental-mode "chordpro"
   "Major mode for editing pro files with guitra chords."
   (set (make-local-variable 'font-lock-defaults)
        '(chordpro-font-lock-keywords
          t   ;; bool - t = Do not fontify using also syntax table.
          t   ;; bool - t = Do case insensitive matching when fontifying.
          ))
   (font-lock-mode t)
   (font-lock-fontify-buffer)
   (setq case-fold-search nil)
   (setq fill-column 71))

(provide 'chordpro-mode)
